-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
-- -----------------------------------------------------
-- Schema kajan
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema kajan
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `kajan` DEFAULT CHARACTER SET latin1 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`kajan`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`kajan` (
  `id` INT NOT NULL,
  `gmailid` VARCHAR(45) NULL,
  `address` VARCHAR(20) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`student`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`student` (
  `name` VARCHAR(45) BINARY NOT NULL,
  `id` INT NULL,
  `gmailid` VARCHAR(45) NULL,
  `kajan_id` INT NOT NULL,
  PRIMARY KEY (`name`, `kajan_id`),
  INDEX `fk_student_kajan_idx` (`kajan_id` ASC) VISIBLE,
  CONSTRAINT `fk_student_kajan`
    FOREIGN KEY (`kajan_id`)
    REFERENCES `mydb`.`kajan` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

USE `kajan` ;

-- -----------------------------------------------------
-- Table `kajan`.`student`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `kajan`.`student` (
  `name` VARCHAR(10) NULL DEFAULT NULL,
  `id` INT(20) NULL DEFAULT NULL,
  `gmaiid` VARCHAR(20) NULL DEFAULT NULL)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
